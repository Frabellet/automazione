# Automazione
## Italian Version

Ciao, questa è la repository dedicata alla Laurea Triennale in Ingegneria dell'Automazione (Polimi) gestita dal PoliNetwork.
Ci troverete materiale (appunti, esercizi ...) riguardante i vari corsi e potrete caricarlo anche voi attraverso il bot telegram [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- Primo anno https://gitlab.com/polinetwork/automazione1y
 
- Secondo anno https://gitlab.com/polinetwork/automazione2y
 
- Terzo anno https://gitlab.com/polinetwork/automazione3y


`DISCLAIMER`
**Non puoi caricare materiale protetto da copyright o qualsiasi altra forma di protezione del diritto d'autore in questa repository né in quelle ad essa associate.**

PoliNetwork non si assume alcuna responsabilità, né approva, supporta o garantisce, espressamente o implicitamente, la completezza, veridicità, accuratezza e affidabilità di alcun contenuto in questa repository né in quelle ad essa associate.


## English Version

Hello everyone, this is the repository of BSc in Automation Engineering (Polimi) maintained by PoliNetwork.
You will be able to find and upload materials for your courses (notes, exercises, ...).
To upload files use the Telegram Bot [@PoliMaterial_bot](https://t.me/PoliMaterial_bot)

- Primo anno https://gitlab.com/polinetwork/automazione1y
 
- Secondo anno https://gitlab.com/polinetwork/automazione2y
 
- Terzo anno https://gitlab.com/polinetwork/automazione3y


`DISCLAIMER`
**You mustn't upload any copyright protected or in any other author protection form covered file on this repository or any of the ones associated to it.**

PoliNetwork takes no responsibility for and we do not expressly or implicitly endorse, support, or guarantee the completeness, truthfulness, accuracy, or reliability of any of the content on this repository or any of the ones associated to it.
